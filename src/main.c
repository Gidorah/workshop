/*
** EPITECH PROJECT, 2019
** workshop
** File description:
** The program entry point.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "workshop.h"

int main(int argc, char *argv[])
{
    /**
     * Three arguments are at least required.
     */
    if (argc <= 3) {
        fprintf(stderr, "%s\n", "Too few arguments!");

        return 84;
    }

    /**
     * The first two arguments must be "gitlab" and "is", otherwise the program will exit.
     */
    if (0 != strcmp(argv[1], "gitlab") || 0 != strcmp(argv[2], "is")) {
        fprintf(stderr, "%s\n", "Hmmmmm...");

        return 84;
    }

    /**
     * Now, based on the last word we will return.
     */
    if (0 == strcmp(argv[3], "cool") || 0 == strcmp(argv[3], "kewl")) {
        fprintf(stdout, "%s\n", "Yay!");

        return 0;
    }

    fprintf(stderr, "%s\n", "So GitLab is not cool, why? :(");

    return 84;
}
